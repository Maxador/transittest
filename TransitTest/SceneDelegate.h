//
//  SceneDelegate.h
//  TransitTest
//
//  Created by Maxime Mongeau on 2019-11-24.
//  Copyright © 2019 Maxime Mongeau. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

