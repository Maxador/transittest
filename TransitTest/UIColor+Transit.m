//
//  UIColor+Transit.m
//  TransitTest
//
//  Created by Maxime Mongeau on 2019-11-25.
//  Copyright © 2019 Maxime Mongeau. All rights reserved.
//

#import "UIColor+Transit.h"

@implementation UIColor (Transit)
+ (UIColor *)colorwithHexString:(NSString *)hex {
    unsigned int hexValue = 0;

    NSScanner *scanner = [NSScanner scannerWithString:hex];
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    [scanner scanHexInt:&hexValue];

    return [UIColor
            colorWithRed:((CGFloat) ((hexValue & 0xFF0000) >> 16)) / 255
            green:((CGFloat) ((hexValue & 0xFF00) >> 8)) / 255
            blue:((CGFloat) ((hexValue & 0xFF))) / 255
            alpha:1];
}

+ (UIColor *)colorForCountryCode:(NSString *)code {
    NSDictionary *colors = @{
        @"CA": @"f44336",
        @"US": @"e040fb",
        @"FR": @"3f51b5",
        @"GB": @"8bc34a",
        @"DE": @"ffc107"
    };

    NSString *hex = [colors objectForKey:code];
    if (hex) {
        return [UIColor colorwithHexString:hex];
    }

    return [UIColor colorwithHexString:@"00bcd4"];
}

@end
