//
//  UIColor+Transit.h
//  TransitTest
//
//  Created by Maxime Mongeau on 2019-11-25.
//  Copyright © 2019 Maxime Mongeau. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (Transit)
+ (UIColor *)colorwithHexString:(NSString *)hex;
+ (UIColor *)colorForCountryCode:(NSString *)code;
@end

NS_ASSUME_NONNULL_END
