//
//  FeedDetailViewController.h
//  TransitTest
//
//  Created by Maxime Mongeau on 2019-11-26.
//  Copyright © 2019 Maxime Mongeau. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Feed.h"

NS_ASSUME_NONNULL_BEGIN

@interface FeedDetailViewController : UIViewController <UINavigationControllerDelegate>

@property Feed *feed;

- (instancetype) initWithFeed:(Feed *)feed;

@end

NS_ASSUME_NONNULL_END
