//
//  FeedDetailViewController.m
//  TransitTest
//
//  Created by Maxime Mongeau on 2019-11-26.
//  Copyright © 2019 Maxime Mongeau. All rights reserved.
//

#import "FeedDetailViewController.h"

@interface FeedDetailViewController ()

@end

@implementation FeedDetailViewController

- (instancetype)initWithFeed:(Feed *)feed {
    self = [super init];
    if (self) {
        self.feed = feed;
    }

    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    CGFloat padding = 20;

    self.title = self.feed.location;

    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc]
                                     initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                     target:self
                                     action:@selector(doneButtonTapped)];

    [self.navigationItem setRightBarButtonItem:doneButton];

    UILabel *cityLabel = [[UILabel alloc] init];
    UILabel *nameLabel = [[UILabel alloc] init];

    cityLabel.text = @"City:";
    nameLabel.text = @"Name:";

    UILabel *cityValueLabel = [[UILabel alloc] init];
    cityValueLabel.numberOfLines = 0;
    UILabel *feedNameValueLabel = [[UILabel alloc] init];
    feedNameValueLabel.numberOfLines = 0;

    cityValueLabel.text = self.feed.location;
    feedNameValueLabel.text = self.feed.feedName;

    [self.view addSubview:cityLabel];
    [self.view addSubview:nameLabel];
    [self.view addSubview:cityValueLabel];
    [self.view addSubview:feedNameValueLabel];

    self.view.backgroundColor = [UIColor whiteColor];

    cityLabel.translatesAutoresizingMaskIntoConstraints = NO;
    nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    cityValueLabel.translatesAutoresizingMaskIntoConstraints = NO;
    feedNameValueLabel.translatesAutoresizingMaskIntoConstraints = NO;
    [cityLabel
     setContentCompressionResistancePriority:UILayoutPriorityRequired
     forAxis:UILayoutConstraintAxisHorizontal];
    [nameLabel
    setContentCompressionResistancePriority:UILayoutPriorityRequired
    forAxis:UILayoutConstraintAxisHorizontal];

    [cityLabel.topAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.topAnchor constant:padding].active = YES;
    [cityLabel.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:padding].active = YES;
    [cityValueLabel.topAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.topAnchor constant:padding].active = YES;
    [cityValueLabel.leftAnchor constraintEqualToAnchor:cityLabel.rightAnchor constant:padding].active = YES;
    [cityValueLabel.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-padding].active = YES;
    [nameLabel.topAnchor constraintEqualToAnchor:cityLabel.bottomAnchor constant:padding].active = YES;
    [nameLabel.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:padding].active = YES;
    [feedNameValueLabel.leftAnchor constraintEqualToAnchor:nameLabel.rightAnchor constant:padding].active = YES;
    [feedNameValueLabel.topAnchor constraintEqualToAnchor:cityValueLabel.bottomAnchor constant:padding].active = YES;
    [feedNameValueLabel.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-padding].active = YES;

    [cityValueLabel sizeToFit];
    [feedNameValueLabel sizeToFit];
}

- (void)doneButtonTapped {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
