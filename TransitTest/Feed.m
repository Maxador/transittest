//
//  Feed.m
//  TransitTest
//
//  Created by Maxime Mongeau on 2019-11-24.
//  Copyright © 2019 Maxime Mongeau. All rights reserved.
//

#import "Feed.h"

@implementation Feed

- (instancetype)initWithId:(int)feedId name:(NSString *)name countryCode:(NSString *)code location:(NSString *)location latitude:(double)latitude longitude:(double) longitude {
    self = [super init];
    if (self) {
        self.feedId = feedId;
        self.feedName = name;
        self.countryCode = code;
        self.location = location;

        self.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    }
    return self;
}

@end
