//
//  ViewController.m
//  TransitTest
//
//  Created by Maxime Mongeau on 2019-11-24.
//  Copyright © 2019 Maxime Mongeau. All rights reserved.
//

#import "ViewController.h"
#import "Feed.h"
#import "UIColor+Transit.h"
#import "UIImage+Transit.h"
#import "FeedDetailViewController.h"

@interface ViewController ()

@property NSURLSession *session;
@property NSURL *feedsUrl;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.session = NSURLSession.sharedSession;
    self.feedsUrl = [[NSURL alloc] initWithString:@"https://api.transitapp.com/v3/feeds?detailed=1"];
    self.mapView = [[MKMapView alloc] init];
    self.mapView.delegate = self;
    [self.mapView registerClass:[MKAnnotationView class] forAnnotationViewWithReuseIdentifier:@"TransitPinView"];

    [self requestFeeds];

    [self setupView];
}

- (void)setupView {
    self.mapView.translatesAutoresizingMaskIntoConstraints = NO;
    self.view.translatesAutoresizingMaskIntoConstraints = NO;

    [self.view addSubview: self.mapView];

    [self.mapView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    [self.mapView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    [self.mapView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.mapView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
}

- (void)requestFeeds {
    [[self.session
      dataTaskWithURL:self.feedsUrl
      completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSError *jsonError;
        id jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];

        if (jsonError) {
            NSLog(@"json serialization error %@", [jsonError localizedDescription]);
            return;
        }

        NSArray *feedsData = [jsonResponse valueForKey:@"feeds"];
        NSMutableArray<Feed *> *feeds = [[NSMutableArray alloc] init];
        for (id feed in feedsData) {
            id bounds = [feed objectForKey:@"bounds"];
            double minLat = [[bounds valueForKey:@"min_lat"] doubleValue];
            double maxLat = [[bounds valueForKey:@"max_lat"] doubleValue];
            double minLon = [[bounds valueForKey:@"min_lon"] doubleValue];
            double maxLon = [[bounds valueForKey:@"max_lon"] doubleValue];

            double meanLat = [self meanDegree:minLat andMax:maxLat];
            double meanLon = [self meanDegree:minLon andMax:maxLon];

            Feed *feedObj = [[Feed alloc]
                             initWithId:[[feed objectForKey:@"id"] intValue]
                             name:[feed objectForKey:@"name"]
                             countryCode:[feed objectForKey:@"country_code"]
                             location:[feed objectForKey:@"location"]
                             latitude:meanLat longitude:meanLon];
            [feeds addObject:feedObj];
        }

        self.feeds = feeds;

        dispatch_async(dispatch_get_main_queue(), ^{
            [self.mapView addAnnotations:self.feeds];
        });
    }] resume];
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    Feed *feed = (Feed *) annotation;
    NSString *identifier = @"TransitPinView";

    MKAnnotationView *annotationView = [self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (annotationView) {
        UIColor *countryColor = [UIColor colorForCountryCode:[feed countryCode]];
        UIImage *pinImage = [[[UIImage imageNamed:@"pin@2x.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] imageWithReplacedColor:countryColor];
        annotationView.image = pinImage;
    }

    return annotationView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    Feed *feed = (Feed *) view.annotation;

    FeedDetailViewController *vc = [[FeedDetailViewController alloc] initWithFeed:feed];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];

    [self presentViewController:nav animated:YES completion:^{
        self.mapView.selectedAnnotations = @[];
    }];
}


#pragma mark - Helpers
- (double)meanDegree:(double)min andMax:(double)max {
    return (min + max) / 2;
}
@end
