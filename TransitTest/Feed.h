//
//  Feed.h
//  TransitTest
//
//  Created by Maxime Mongeau on 2019-11-24.
//  Copyright © 2019 Maxime Mongeau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Feed : NSObject <MKAnnotation>

@property int feedId;
@property NSString *city; // Could be fed by some Reverse Geocoding on the detail presentation.
@property NSString *feedName;
@property NSString *countryCode;
@property NSString *location;
@property(nonatomic) CLLocationCoordinate2D coordinate;

- (instancetype)initWithId:(int)feedId name:(NSString *)name countryCode:(NSString *)code location:(NSString *)location latitude:(double)latitude longitude:(double) longitude;

@end

NS_ASSUME_NONNULL_END
