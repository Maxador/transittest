//
//  ViewController.h
//  TransitTest
//
//  Created by Maxime Mongeau on 2019-11-24.
//  Copyright © 2019 Maxime Mongeau. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Feed.h"

@interface ViewController : UIViewController <MKMapViewDelegate>

@property MKMapView *mapView;
@property NSArray<Feed *> *feeds;

@end

