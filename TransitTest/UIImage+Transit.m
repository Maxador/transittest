//
//  UIImage+Transit.m
//  TransitTest
//
//  Created by Maxime Mongeau on 2019-11-25.
//  Copyright © 2019 Maxime Mongeau. All rights reserved.
//

#import "UIImage+Transit.h"

@implementation UIImage (Transit)
- (UIImage *)imageWithReplacedColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, self.size.width, self.size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, self.scale);
    CGContextRef c = UIGraphicsGetCurrentContext();
    [self drawInRect:rect];
    CGContextSetFillColorWithColor(c, [color CGColor]);
    CGContextSetBlendMode(c, kCGBlendModeSourceAtop);
    CGContextFillRect(c, rect);
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return tintedImage;
}

@end
